section.data

newline_char: db 10


section .text
global exit
global string_length
global print_string
global print_newline
global print_char
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
 
 
; Принимает код возврата и завершает текущий процесс
exit:  
    mov rax, 60                 ; Запись кода системного вызова 
    syscall                     ; Системный вызов

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax                ; Обнуление счетчика длины
    .loop: 
        cmp byte[rax+rdi], 0    ; Является ли символь нуль-терминатором
        je .end                 ; Если да, то конец
        inc rax                 ; Если нет, то прибавляем к длине 1
        jmp .loop               ; Повторяем цикл
    .end:
        ret                     ; Выход из подпрограммы

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length          ; Вызов подпрограммы для нахождения длины строки
    mov rdx, rax                ; Помещаем длину строки в rdx
    pop rsi                     ; Помещаем в rsi адрес начала строки
    mov rax, 1                  ; Запись кода системного вызова 
    mov rdi, 1                  ; Дескриптор вывода
    syscall                     ; Системный вызов
    ret                         ; Выход из подпрограммы

; Принимает код символа и выводит его в stdout
print_char:
    mov rdx, 1                  ; Помещаем 1 (длина символа) в rdx
    mov rax, 1                  ; Запись кода системного вызова 
    push rdi
    mov rdi, 1                  ; Дескриптор вывода
    mov rsi, rsp                ; Помещаем в rsi адрес символа через указатель на стек
    syscall                     ; Системный вызов
    add rsp, 8
    ret                         ; Выход из подпрограммы

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rax, 1                  ; Запись кода системного вызова 
    mov rdi, 1                  ; Дескриптор вывода
    mov rsi, newline_char       ; Помещаем в rsi адрес символа
    mov rdx, 1                  ; Помещаем 1 (длина символа) в rdx
    syscall                     ; Системный вызов
    ret                         ; Выход из подпрограммы

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rcx, 10                 ; Сохраняем основание системы
    mov r9, rsp                 ; Сохраняем указатель на вершину стека
    mov rax, rdi                ; Помещаем число в rax
    push 0                      ; Конец строки
    .loop:
        xor rdx, rdx            ; Обнуляем регистр
        div rcx                 ; Целая часть записывается в rax, остаток в rdx
        add rdx, '0'            ; Переводим в ASCII
        dec rsp                 ; Уменьшаем указатель стека
        mov byte[rsp], dl       ; Записываем rdx на вершину стека
        test rax, rax           ; Проверяем на конец строки 
        je .end                 ; Если конец, то завершаем
        jmp .loop               ; Если нет, то повторяем цикл

    .end:
        mov rdi, rsp            ; Помещаем адрес первого символа
        push r9
        call print_string       ; Вызов подпрограммы для вывода строки
        pop r9
        mov rsp, r9             ; Восстанавливаем значение rsp
        ret                     ; Выход из подпрограммы

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi               ; Сравниваем число с 0
    jge .pos                    ; Переход на метку pos, если число больше равно 0
    push rdi                    ; Иначе сохраняем число
    mov rdi, '-'                ; Выводим знак минус
    call print_char
    pop rdi
    neg rdi                     ; Преобразуем в положительное число
    jmp print_uint              ; Выводим его
    .pos:
        jmp print_uint          ; Выводим неотрицательное число

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx                ; Обнуляем регистры
    xor r10, r10 
    xor r11, r11
    .loop:
        mov r10b, byte[rdi+rcx] ; Помещаем символ в r10b
        mov r11b, byte[rsi+rcx] ; Помещаем символ в r11b
        cmp r10b, r11b          ; Сравниваем их
        jne .false              ; Если не равно, переходим на метку false
        test r10b, r10b
        je .true                ; Если встречаем нуль-терминатор, значит все символы совпали
        inc rcx                 ; Увеличиваем счетчик
        jmp .loop
    .true:
        mov rax, 1              ; Возвращаем 1
        ret
    .false:
        xor rax, rax            ; Возвращаем 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax                ; Запись кода системного вызова
    xor rdi,rdi                 ; Дескриптор ввода
    mov rdx, 1                  ; Помещаем 1 (длина символа) в rdx
    push 0                      ; Символ помещаем на стек
    mov rsi, rsp                ; Помещаем адрес символа в rsi
    syscall                     ; Системный вызов
    pop rax                     ; Помещаем введенный символ в rax
    ret                         ; Выход из подпрограммы

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    xor rcx, rcx                ; Обнуление регистров
    xor rax, rax
    push r14
	mov r14, rsi
	push r15
	mov r15, rdi
    .loop:
        push rcx
        call read_char          ; Чтение символа
        pop rcx

        cmp rax, 0x20           ; Проверка на пробел
        je .whitespace
        cmp rax, 0x9            ; Проверка на табуляцию
        je .whitespace
        cmp rax, 0xA            ; Проверка на перенос строки
        je .whitespace

        mov [r15+rcx], rax      ; Помещаем символ в буфер

        test rax, rax           ; Проверка на конец строки
		jz .end

        inc rcx                 ; Увеличиваем счетчик
        cmp rcx, r14            ; Проверка на переполнение буфера
        je .error

        jmp .loop               ; Повторение цикла

    .whitespace:
        test rcx, rcx              ; Если пробельный символ в начале слова, то пропускаем
        je .loop
        jmp .end                ; Если пробельный символ после слова, то переходим в конец
    .error:                                                                
        xor rax, rax            ; Возвращаем ноль из-за переполнения буфера
        pop r14
        pop r15
        ret
    .end:
        xor rax, rax
        mov [r15+rcx], rax      ; Добавляем нуль-терминатор в конце
        mov rax, r15            ; Помещаем адрес буфера в rax
        mov rdx, rcx            ; Помещаем длину буфера в rdx
        pop r14
        pop r15
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax                ; Обнуляем регистры
    xor rcx, rcx
    mov r8, 10
    .loop:
        movzx r9, byte[rdi+rcx] ; Расширяет нулями
        test r9, r9             ; Если ноль, то конец
        je .end
        cmp r9b, '0'            ; Проверка на цифру
        jl .end
        cmp r9b, '9'
        jg .end

        mul r8
        sub r9b, '0'            ; Получаем цифру
        add rax, r9             ; Добавляем цифру
        inc rcx                 ; Увеличиваем счетчик
        jmp .loop               ; Повторяем цикл

    .end:
        mov rdx, rcx            ; Помещаем длину в rdx
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax                ; Обнуляем регистры
    xor rdx, rdx
    mov rcx, rdi
    cmp byte[rcx], '-'          ; Если отрицательное, то переход на метку neg
    je .neg
    mov rdi, rcx
    jmp parse_uint              ; Если положительное, то переходим в подпрограмму 
    ret                         ; для считывания беззнакого числа
    .neg:
        inc rcx                 ; Переход на следующий за минусом символ
        mov rdi, rcx
        push rcx
        call parse_uint         ; Считываем положительное число
        pop rcx
        neg rax                 ; Меняем знак
        inc rdx                 ; Добавляем к длине слова длину знака
        ret


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax                ; Обнуляем регистры
    xor rcx, rcx
    push rsi
    push rdi
    push rcx
    push rdx
    call string_length
    pop rdx
    pop rcx
    pop rdi
    pop rsi

    mov r9, rax                 ; Помещаем в r8 длину строки
    cmp rdx, r9                 ; Сравниваем длину буфера и длину строки
    jl .err
    .loop:
        cmp rcx, r9             ; Если счетчик больше длины, то конец
        jg .end
        mov r10, [rdi+rcx]      ; Помещаем символ из строки в r10
        mov [rsi+rcx], r10      ; Помещаем символ из r10 в буфер
        inc rcx                 ; Увеличиваем счетчик
        jmp .loop               ; Повторяем цикл
    
    .err:
        xor rax, rax            ; Возврат 0 при ошибке
        ret
    .end:
        mov rax, r9             ; Помещаем длину в rax
        ret
