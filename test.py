import subprocess

path = "./main"

class Test:
    def __init__(self, num):
        self.correctError = ""
        self.correctOutput = ""
        self.num = num
    def setInput(self, testInput):
        self.testInput = testInput
        return self
    def setCorrectOutput(self, correctOutput):
        self.correctOutput = correctOutput
        return self
    def setCorrectError(self, correctError):
        self.correctError = correctError
        return self

tests = [Test(1).setInput("first").setCorrectOutput("first word"),
        Test(2).setInput("second").setCorrectOutput("second word"),
        Test(3).setInput("third").setCorrectOutput("third word"),
        Test(4).setInput("str").setCorrectError("String not found"),
        Test(5).setInput("").setCorrectError("String not found"),
        Test(6).setInput("12345").setCorrectError("String not found"),
        Test(7).setInput("T"*260).setCorrectError("String too large"),
        Test(8).setInput("Two words").setCorrectError("String not found")]

print("Running " + str(len(tests)) + " tests")
print("----------------------------")

for i in range(len(tests)):

    result = subprocess.Popen([path], stdin = subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    programOutput, programError = result.communicate(input = tests[i].testInput.encode())

    programOutput = programOutput.decode().strip()
    programError = programError.decode().strip()

    if programOutput == tests[i].correctOutput and programError == tests[i].correctError:
        print(f'Test {i+1} +')
    else:
        print(f'Test {i+1} failed strout: "{programOutput}", strerr: "{programError}", expected: strout: "{tests[i].correctOutput}", strerr: "{tests[i].correctError}"')

print('\n')
