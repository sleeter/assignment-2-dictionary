%define prev 0
%macro colon 2
    %ifid %2
        %ifstr %1
            %2: 
                dq prev
                db %1, 0
            %define prev %2
        %else
            %error "Первый аргумент не является идентификатором"
        %endif
    %else
        %error "Второй аргумент не является строкой"
    %endif
%endmacro
