clean:
 rm *.o main

build: main

rebuild: clean build

test:
 python3 test.py

lib.o: lib.asm
 nasm -g lib.asm -f elf64 -o lib.o

dict.o: dict.asm lib.inc
 nasm -g dict.asm -f elf64 -o dict.o

main.o: main.asm words.inc dict.inc lib.inc
 nasm -g main.asm -f elf64 -o main.o

main: main.o lib.o dict.o
 ld -o main lib.o dict.o main.o

.PHONY: clean build rebuild test
