%include "lib.inc"

 
section .text

global find_word
 
find_word:
        push rsi
        push rdi

        .next:
                add rsi, 8
                call string_equals
		mov r8, rax
                sub rsi, 8
		cmp r8, 1
                je .success
 
        cmp qword[rsi], 0
        je .fail
        mov rsi, qword[rsi]
        jmp .next
 
        .fail:
                xor rax, rax
                pop rdi
                pop rsi
                ret
        .success:
                mov rax, rsi
                pop rdi
                pop rsi
                ret
