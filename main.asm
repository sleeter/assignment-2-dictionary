%include "words.inc"
%include "lib.inc"
%include "dict.inc"
%define BUFFER_SIZE 256
%define SYS_WRITE 1
%define STD_ERR 2

section .rodata
    read_error: db 'String too large', 0xA, 0
    key_not_found: db 'String not found', 0xA, 0
    newline_char: db 10

section .bss
    buf: resb BUFFER_SIZE

section .text

global _start

_start:
    mov rsi, 256
    mov rdi, buf
    call read_word
    test rax, rax
    jz .reading_error

    mov rdi, buf
    mov rsi, first_word
    call find_word
    test rax, rax
    jz .not_found
    
    mov rdi, rax
    add rdi, 8
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    add rdi, 1
    call print_string
    push rdi
    mov rdi, newline_char
    call print_char
    pop rdi
    xor rdi, rdi
    jmp exit
    
    .reading_error:
        mov rdi, read_error
        jmp .print_error
 
    .not_found:
        mov rdi, key_not_found

    .print_error:
	    push rdi
	    call string_length
	    pop rsi
	    mov rdx, rax
	    mov rax, SYS_WRITE
	    mov rdi, STD_ERR
	    syscall
	    jmp exit
